package net.gikos.testapp;

import com.sforce.soap.partner.*;
import com.sforce.soap.partner.sobject.*;
import com.sforce.ws.*;

public class ChangePassword {

    public static void main(String[] args) {
        String userName = "kranium@gikos.net";
        String passwd = "ChangeSoon2013";
        String token =  "FCLY4huqLdA7msHrSHXVvj1m";
        String newPasswd = "newPassword2013";

        ConnectorConfig config = new ConnectorConfig();
        config.setUsername(userName);
        config.setPassword(passwd+token);

        PartnerConnection connection = null;

        try {

            // create a connection object with the credentials
            connection = Connector.newConnection(config);

            // Print user and session info
            GetUserInfoResult userInfo = connection.getUserInfo();

            System.out.println("UserID: " + userInfo.getUserId());

            //Coundnt find anything under userInfo.* for the isActive flag, using this query instead
            QueryResult queryResults = connection.query("SELECT Id, Name, IsActive from User " +
                    "WHERE Id= '" + userInfo.getUserId() + "'");

            if (queryResults.getSize() > 0) {
                for (SObject s: queryResults.getRecords()) {
                    System.out.println("Id: " + s.getField("Id") + " - Name: "+s.getField("Name") + " - IsActive: "+s.getField("IsActive"));
                }
            }

            SetPasswordResult result = connection.setPassword(userInfo.getUserId(), newPasswd);
            System.out.println("The password for user ID " + userName + " changed to " + newPasswd);

        } catch (ConnectionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}