package net.gikos.testapp;

import com.sforce.soap.partner.*;
import com.sforce.soap.partner.sobject.*;
import com.sforce.ws.*;

public class AddUser {

    public static void main(String[] args) {

        ConnectorConfig config = new ConnectorConfig();
        config.setUsername("kranium@gikos.net");
        config.setPassword("ChangeSoon2013FCLY4huqLdA7msHrSHXVvj1m");

        PartnerConnection connection = null;

        try {

            // create a connection object with the credentials
            connection = Connector.newConnection(config);

            // create a new account
            System.out.println("Creating a new Account...");
            SObject account = new SObject();
            account.setType("Account");
            account.setField("Name", "Kranium Test 1");
            SaveResult[] results = connection.create(new SObject[] { account });
            System.out.println("Created Account: " + results[0].getId());

            // query for the 5 newest accounts
            System.out.println("Querying for the 5 newest Accounts...");
            QueryResult queryResults = connection.query("SELECT Id, Name from Account " +
                    "ORDER BY CreatedDate DESC LIMIT 5");
            if (queryResults.getSize() > 0) {
                for (SObject s: queryResults.getRecords()) {
                    System.out.println("Id: " + s.getField("Id") + " - Name: "+s.getField("Name"));
                }
            }

        } catch (ConnectionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}